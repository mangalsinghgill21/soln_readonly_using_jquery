﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage1.aspx.cs" Inherits="Soln_ReadOnly_Using_Jquery.WebPage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Using jQuery Readonly</title>
    <link href="css/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js"></script>

    <script type="text/javascript">

        function pageLoad()
        {
            $(document).ready(function ()
            {
                $('#<% = txtfirstname.ClientID%>').val('Mangal Singh');
                $('#<% = txtlastname.ClientID%>').val('Gill');
                $('#<% = txtage.ClientID%>').val('21');

                $("#<%=btnedit.ClientID%>").click(function ()
                {
                    $('#<% = txtfirstname.ClientID%>').attr('readonly', 'false');
                    $('#<% = txtlastname.ClientID%>').attr('readonly', 'false');
                    $('#<% = txtage.ClientID%>').attr('readonly', 'false');

                });

                $("#<%=btnsubmit.ClientID%>").click(function ()
                {
                    window.location.href = "WebPage2.aspx";
                 });

            });

        }
   </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                 <asp:TextBox ID ="txtfirstname" runat="server" ReadOnly="true" placeholder="FirstName" CssClass="txt" /></br>
               
                <asp:TextBox ID ="txtlastname" runat="server" ReadOnly="true" placeholder="LastName" CssClass="txt" /></br>
               
                 <asp:TextBox ID ="txtage" runat="server" ReadOnly="true" placeholder="Age" CssClass="txt" /></br>
               
                 <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="btn btn2" />

                <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn2" />
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
